using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class goblinMoves : MonoBehaviour
{
    private bool walk;
    private bool jump;
    private Vector3 posPrec;
    private Vector3 centerOfTable;
    private Animator anim;
    private bool chestFound;
    private float maxSpeed;

    public float speed;
    public GameObject chest;
    public Transform ImageTarget;
    public float walkThreshold;

    void Start()
    {
        walk = false;
        jump = false;
        chestFound = false;
        walkThreshold = 0.005f;
        anim = gameObject.GetComponent<Animator>();
        posPrec = gameObject.transform.position;
        // barre de vitesse
        speed = 0.0f;
    }
    void Update()
    {
        //position et rotation de Styx fix� sur plan z=0
        transform.position = new Vector3(-ImageTarget.position.x, 0, -ImageTarget.position.z);
        transform.eulerAngles = new Vector3(0, ImageTarget.transform.eulerAngles.y, 0);
        speed = speed / 2 + (transform.position - posPrec).magnitude / 2; // moyenne de vitesse sur deux frames.
        //tomb� ?
        if (transform.position.z < 11 && transform.position.z > 2 && (transform.position.x < -6 || transform.position.x > -2))
        {
            Debug.Log("you are dead");
            Destroy(gameObject, 1);
        }
        //d�couverte du tr�sor
        if (!chestFound)
        {
            if ((transform.position - chest.transform.position).magnitude <= 2.0f)
            {
                chestFound = true;
                Destroy(chest);
            }
        }
        //animations
        if ((transform.position - posPrec).magnitude > walkThreshold)
        {
            //Debug.Log("walking");
            walk = true;
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //Debug.Log("jumping");
            jump = true;
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            jump = false;
        }
        else
        {
            //Debug.Log("idle");
            walk = false;
        }
        anim.SetBool("isWalk", walk);
        anim.SetBool("isJump", jump);
        posPrec = transform.position;
    }
}
